﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPos.DataModel;
using XPos.ViewModel;

namespace XPos.DataAccess
{
    public class VariantRepo
    {
        // Retrieve/Select/Get
        public static List<VariantViewModel> All()
        {
            //List<VariantViewModel> result = new List<VariantViewModel>();
            //using (var db = new XPosContext())
            //{
            //    result = db.Variants
            //        .Select(c => new VariantViewModel
            //        {
            //            Id = c.Id,
            //            CategoryId = c.CategoryId,
            //            CategoryName = c.Category.Name,
            //            Initial = c.Initial,
            //            Name = c.Name,
            //            Active = c.Active
            //        }).ToList();
            //}
            return ByCategory(-1);
        }

        public static List<VariantViewModel> ByCategory(long id)
        {
            // id => Category Id
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Variants
                    .Where(v => v.CategoryId == (id == -1 ? v.CategoryId : id))
                    .Select(c => new VariantViewModel
                    {
                        Id = c.Id,
                        CategoryId = c.CategoryId,
                        CategoryName = c.Category.Name,
                        Initial = c.Initial,
                        Name = c.Name,
                        Active = c.Active
                    }).ToList();
            }
            return result;
        }

        // Retrieve/Select/Get by Id
        public static VariantViewModel ById(long id)
        {
            VariantViewModel result = new VariantViewModel();
            using (var db = new XPosContext())
            {
                result = db.Variants
                    .Where(c => c.Id == id)
                    .Select(c => new VariantViewModel
                    {
                        Id = c.Id,
                        CategoryId = c.CategoryId,
                        CategoryName = c.Category.Name,
                        Initial = c.Initial,
                        Name = c.Name,
                        Active = c.Active
                    })
                    .FirstOrDefault();

                if (result == null)
                    result = new VariantViewModel();
            }
            return result;
        }

        // Update => Insert & Edit
        public static ResponseResult Update(VariantViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    //Insert => Id = 0
                    if (entity.Id == 0)
                    {
                        Variant var = new Variant();
                        var.Initial = entity.Initial;
                        var.CategoryId = entity.CategoryId;
                        var.Name = entity.Name;
                        var.Active = entity.Active;

                        var.CreatedBy = "Lunghi";
                        var.CreatedDate = DateTime.Now;

                        db.Variants.Add(var);
                        db.SaveChanges();

                        result.Entity = var;
                    }
                    //Edit => Id != 0
                    else
                    {
                        Variant var = db.Variants
                            .Where(c => c.Id == entity.Id)
                            .FirstOrDefault();

                        if (var != null)
                        {
                            var.CategoryId = entity.CategoryId;
                            var.Initial = entity.Initial;
                            var.Name = entity.Name;
                            var.Active = entity.Active;

                            var.ModifiedBy = "Lunghi";
                            var.ModifiedDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Category not found";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }

        public static ResponseResult Delete(long id)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    Variant var = db.Variants.Where(c => c.Id == id).FirstOrDefault();
                    if (var != null)
                    {
                        VariantViewModel entity = new VariantViewModel();
                        entity.Id = var.Id;
                        entity.CategoryId = var.CategoryId;
                        entity.CategoryName = var.Category.Name;

                        entity.Initial = var.Initial;
                        entity.Name = var.Name;
                        entity.Active = var.Active;

                        db.Variants.Remove(var);
                        db.SaveChanges();
                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Variant not found";
                    }

                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
