﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPos.DataModel;
using XPos.ViewModel;

namespace XPos.DataAccess
{
    public class ProductRepo
    {
        public static Tuple<List<ProductViewModel>, int> All(int page, int cnt)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            int rowNum = 0;
            using (var db = new XPosContext())
            {
                var query = db.Products;
                rowNum = query.Count();
                result = query
                    .OrderBy(p => p.Id)
                    .Skip((page - 1) * cnt)
                    .Take(cnt)
                    .Select(c => new ProductViewModel
                    {
                        Id = c.Id,
                        VariantId = c.VariantId,
                        VariantName = c.Variant.Name,
                        CategoryId = c.Variant.CategoryId,
                        CategoryName = c.Variant.Category.Name,
                        Initial = c.Initial,
                        Name = c.Name,
                        Description = c.Description,
                        Price = c.Price,
                        Stock = c.Stock,
                        Image = c.Image,
                        Active = c.Active
                    }).ToList();
            }
            return new Tuple<List<ProductViewModel>, int>(result, rowNum);
        }

        public static Tuple<List<ProductViewModel>, int> ByFilter(string search)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            int count = 0;
            using (var db = new XPosContext())
            {
                var query = db.Products
                    .Where(p => p.Initial.Contains(search) || p.Name.Contains(search) || p.Description.Contains(search));
                count = query.Count();
                result = query
                    .Take(5)
                    .Select(c => new ProductViewModel
                    {
                        Id = c.Id,
                        VariantId = c.VariantId,
                        VariantName = c.Variant.Name,
                        CategoryId = c.Variant.CategoryId,
                        CategoryName = c.Variant.Category.Name,
                        Initial = c.Initial,
                        Name = c.Name,
                        Description = c.Description,
                        Price = c.Price,
                        Stock = c.Stock,
                        Image = c.Image,
                        Active = c.Active
                    })
                    .ToList();
            }
            return new Tuple<List<ProductViewModel>, int>(result, count);
        }

        // Retrieve/Select/Get by Id
        public static ProductViewModel ById(long id)
        {
            ProductViewModel result = new ProductViewModel();
            using (var db = new XPosContext())
            {
                result = db.Products
                    .Where(c => c.Id == id)
                    .Select(c => new ProductViewModel
                    {
                        Id = c.Id,
                        VariantId = c.VariantId,
                        VariantName = c.Variant.Name,
                        CategoryId = c.Variant.CategoryId,
                        CategoryName = c.Variant.Category.Name,
                        Initial = c.Initial,
                        Name = c.Name,
                        Description = c.Description,
                        Price = c.Price,
                        Stock = c.Stock,
                        Image = c.Image,
                        Active = c.Active
                    })
                    .FirstOrDefault();

                if (result == null)
                    result = new ProductViewModel();
            }
            return result;
        }

        // Update => Insert & Edit
        public static ResponseResult Update(ProductViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    //Insert => Id = 0
                    if (entity.Id == 0)
                    {
                        Product var = new Product();
                        var.Initial = entity.Initial;
                        var.VariantId = entity.VariantId;
                        var.Name = entity.Name;
                        var.Description = entity.Description;
                        var.Price = entity.Price;
                        var.Stock = entity.Stock;
                        var.Image = entity.Image;
                        var.Active = entity.Active;

                        var.CreatedBy = "Lunghi";
                        var.CreatedDate = DateTime.Now;

                        db.Products.Add(var);
                        db.SaveChanges();

                        result.Entity = var;
                    }
                    //Edit => Id != 0
                    else
                    {
                        Product var = db.Products
                            .Where(c => c.Id == entity.Id)
                            .FirstOrDefault();

                        if (var != null)
                        {
                            var.VariantId = entity.VariantId;
                            var.Initial = entity.Initial;
                            var.Name = entity.Name;
                            var.Description = entity.Description;
                            var.Price = entity.Price;
                            var.Stock = entity.Stock;
                            var.Image = entity.Image;
                            var.Active = entity.Active;

                            var.ModifiedBy = "Lunghi";
                            var.ModifiedDate = DateTime.Now;

                            db.SaveChanges();

                            //Product en = var;
                            //en.OrderDetails = null;
                            //en.Variant = null;
                            result.Entity = new { Initial = entity.Initial, Name = entity.Name };
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Category not found";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
