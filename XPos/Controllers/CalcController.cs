﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace XPos.MVC.Controllers
{
    public class CalcController : Controller
    {
        // GET: Calc
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Addition(long val1, long val2)
        {
            return Json(new
            {
                result = val1 + val2
            }, JsonRequestBehavior.AllowGet);
        }
    }
}