﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPos.MVC.Models;

namespace XPos.MVC.Controllers
{
    public class HtmlController : Controller
    {
        public static List<Employee> listEmployee = new List<Employee>() {
                new Employee() {Id=1, FirstName="Budi", LastName="Wardi", Address="Jakarta", Gender="Male", Age=36 },
                new Employee() {Id=2, FirstName="Wati", LastName="Saraswati", Address="Bandung", Gender="Female", Age=29 }
            };

        // GET: Html
        [HttpGet]
        public ActionResult Index()
        {
            return View("Employees", listEmployee);
        }

        //Get: Create
        public ActionResult Create()
        {
            return View();
        }

        //Post: Create
        [HttpPost]
        public ActionResult Create(Employee model)
        {
            int newId = listEmployee.Max(e => e.Id) + 1;
            model.Id = newId;
            listEmployee.Add(model);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            Employee model = listEmployee.Find(e => e.Id == id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Employee model)
        {
            int idx = listEmployee.FindIndex(e => e.Id == model.Id);
            listEmployee[idx] = model;
            return RedirectToAction("Index");
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Table() {
            ViewBag.Pesan = "This is ViewBag sample.";
            return View();
        }

    }
}